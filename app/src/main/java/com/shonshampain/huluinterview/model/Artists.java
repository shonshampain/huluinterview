package com.shonshampain.huluinterview.model;

import java.util.List;

public class Artists {
    public String href;
    public List<Item> items;
    public int limit;
    public String next;
    public int offset;
    public String previous;
    public int total;
}
