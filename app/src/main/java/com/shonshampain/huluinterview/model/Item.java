package com.shonshampain.huluinterview.model;

import java.util.List;

public class Item {
    public ExternalUrls external_urls;
    public Followers followers;
    public List<String> genres;
    public String href;
    public String id;
    public List<Image> images;
    public String name;
    public int popularity;
    public String type;
    public String uri;
}
