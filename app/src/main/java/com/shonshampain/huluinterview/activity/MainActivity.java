package com.shonshampain.huluinterview.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.shonshampain.huluinterview.R;
import com.shonshampain.huluinterview.model.Image;
import com.shonshampain.huluinterview.model.SpotifyResponse;
import com.shonshampain.huluinterview.rest.ApiService;
import com.shonshampain.huluinterview.rest.RestClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<SpotifyResponse> {

    private ApiService service;
    private SpotifyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              enterSearchText();
            }
        });

        service = new RestClient().getService();

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.spotify_list);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        adapter = new SpotifyAdapter(this);
        recyclerView.setAdapter(adapter);
    }

    private void enterSearchText() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter Search Text");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String text = input.getText().toString().trim();
                if (text.length() > 0) {
                    search(text);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // do nothing
            }
        });
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        search("foo");
    }

    private void search(String term) {
        Call<SpotifyResponse> call = service.search(term, "artist", 10);
        call.enqueue(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Call<SpotifyResponse> call, Response<SpotifyResponse> response) {
        if(response.isSuccessful()) {
            adapter.clear();
            SpotifyResponse sr = response.body();
            for (int i = 0; i < sr.artists.items.size(); i++) {
                List<Image> img = sr.artists.items.get(i).images;
                SpotifyData dataItem = new SpotifyData(
                        sr.artists.items.get(i).name,
                        img.size() > 0 ? img.get(0).url : null
                );
                adapter.add(dataItem);
            }
            adapter.notifyDataSetChanged();
            Log.d("MainActivity", "Successful search, returned " + sr.artists.items.size() + " items");
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<SpotifyResponse> call, Throwable t) {
        t.printStackTrace();
    }
}
