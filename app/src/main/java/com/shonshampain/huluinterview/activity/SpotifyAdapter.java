package com.shonshampain.huluinterview.activity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shonshampain.huluinterview.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

class SpotifyAdapter extends RecyclerView.Adapter<SpotifyViewHolder> {
    private ArrayList<SpotifyData> data = new ArrayList<>();
    private Context context;

    SpotifyAdapter(Context context) {
        this.context = context;
    }

    void add(SpotifyData dataItem) {
        data.add(dataItem);
    }

    void clear() {
        data.clear();
    }

    @Override
    public SpotifyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spotify_item, parent, false);
        return new SpotifyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SpotifyViewHolder holder, int position) {
        SpotifyData dataItem = data.get(position);
        holder.name.setText(dataItem.name);
        Log.d("SpotifyAdapter", "name: [" + dataItem.name + "]");
        Log.d("SpotifyAdapter", "album: [" + dataItem.album + "]");
        if (data.get(position).album != null) {
            Picasso.with(context).
                    load(data.get(position).album).
                    placeholder(R.drawable.ic_sync_black_24dp).
                    error(R.mipmap.ic_no_image).
                    into(holder.album);
        } else {
            Picasso.with(context).
                    load(R.mipmap.ic_no_image).
                    into(holder.album);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
