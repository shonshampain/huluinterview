package com.shonshampain.huluinterview.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shonshampain.huluinterview.R;

public class SpotifyViewHolder extends RecyclerView.ViewHolder {
    public TextView name;
    public ImageView album;

    public SpotifyViewHolder(View itemView) {
        super(itemView);
        name = (TextView)itemView.findViewById(R.id.spotify_item_name);
        album = (ImageView)itemView.findViewById(R.id.spotify_item_album);
    }
}
