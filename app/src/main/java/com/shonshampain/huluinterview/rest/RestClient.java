package com.shonshampain.huluinterview.rest;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RestClient {

    private static final String BASE_URL = "https://api.spotify.com/v1/";
    private ApiService apiService;

    public RestClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        apiService = retrofit.create(ApiService.class);
    }

    public ApiService getService() {
        return apiService;
    }
}