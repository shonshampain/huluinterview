package com.shonshampain.huluinterview.rest;

import com.shonshampain.huluinterview.model.SpotifyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("search")
    Call<SpotifyResponse> search(@Query("q") String q, @Query("type") String type, @Query("limit") int limit);
}
